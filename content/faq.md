---
title: "Faq"
date: 2021-03-11T12:55:13+01:00
draft: false
---

## Qu'est-ce que Mélisse ?

Mélisse est une infrastructure informatique proposant des services web entièrement basés sur des logiciels libres. Le but est de faire la promotion des logiciels libres, en offrant la possibilité aux particuliers comme aux collectivités d'avoir une pratique informatique plus éthique, respectueuse de la vie privée de ses utilisat.eurs.rices. Mélisse est sur le point d'obtenir le statut juridique d'une association loi 1901.

## Pourquoi utiliser Mélisse ?

Le sens d'utiliser une infrastructure comme Mélisse réside dans la volonté de savoir où sont nos données à caractère personnel générées par notre activité sur le web. Le but est d'utiliser des alternatives aux géants du web car nous ne souhaitons pas que nos données soient utilisées à des fins de revente publicitaire ou soient en libre accès à des organismes de surveillance. Utiliser Mélisse, c'est également soutenir la démarche du logiciel libre qui reconnaît Internet et le code source des applications que nous utilisons comme un bien commun, plutôt que comme une opportunité à des groupes commerciaux de faire de l'argent.

## Quels services proposez-vous ?

Pour l'instant, nous proposons une instance de Nextcloud (alternative open-source de stockage de fichiers en ligne, tel que Dropbox par exemple), solution qui permet également une gestion d'agendas et de listes de contacts partagés et synchronisés avec vos différents appareils. Nous proposons aussi une instance de RocketChat, alternative à des services de discussion en ligne tels que slack. Une instance de bitwarden, gestionnaire de mots de passe sécurisé, est en libre accès : contrairement aux autres services, vous pouvez l'utiliser sans être inscrit.e sur Mélisse.

## Qui peut en bénéficier ?

Absolument tout le monde peut être adhérent.e à Mélisse et accéder à ses services. Personne physique ou morale. Nous avons à coeur de proposer nos solutions aux particuliers comme aux collectivités. En effet, nous sommes conscients du besoin grandissant d'outils numériques pour l'organisation du travail, surtout depuis la pandémie de COVID 19. Dans le cas des collectivités, un compte personnel est attribué à chaque personne membre.

## Combien ça me coûte ?

Mélisse étant une association, chaque personne, physique ou morale, désirant utiliser ses services doit être adhérent.e à l'association. Nous avons à coeur que nos services soient accessibles gratuitement. Cependant, gérer une infrastructure informatique ayant des coûts, nous demandons une participation financière (via la cotisation liée à l'adhésion) si vos besoins sont plus grands que les services gratuits. Rassurez-vous, les services proposés gratuitement sont largement corrects. Nous ne sommes pas dans une démarche freemium/premium, mais plutôt dans une participation aux frais si vous voulez soutenir le projet ou si vos besoins sont vraiments significatifs.

##### Particulier sans cotisation, vous avez accès à :

- 10 Go d'espace de stockage sur Nextcloud, ainsi qu'un accès aux agendas et listes de contacts.
- un compte sur RocketChat

##### Collectivité sans cotisation, vous avez accès à:

- La possibilité de déclarer trois adhérent.e.s de Mélisse comme membre de votre collectivité.
- Un dossier partagé de 10Go entre les membres de votre collectivité sur Nextcloud

Vous pouvez, en tant que particulier ou collectivité, décider de payer une cotisation dont le montant est à prix libre (dont le minimum est fixé à 1€ symbolique). **Vous obtenez alors**:
- La possibilité d'augmenter votre espace de stockage sur Nextcloud
- La possibilité de déclarer plus de trois adhérent.e.s comme membres de votre collectivité (dans le cas d'une collectivité)

Le montant conseillé pour les cotisations est de 15€ (renouvellement annuel).

## Si je paie une cotisation, à quoi sert mon argent ?

Mélisse est une association entièrement gérée par des bénévoles. En ce sens, aucune trésorerie ne sert à rémunérer le travail effectué.

Toutefois, Mélisse auto-hébergeant entièrement son infrastructure, l'argent des cotisations sert à acheter du matériel neuf si besoin, ou à entretenir le matériel existant (serveurs, disques durs, ...).

## Si j'utilise vos services, que deviennent mes données à caractère personnel ?

Le respect des données de ses membres est le point central de l'activité de Mélisse. En ce sens, nous vous garantissons que celles-ci ne seront jamais divulgués pour quelle que raison que ce soit (sauf si la loi nous y contraint). C'est également une des raisons principales pour laquelle nous utilisons exclusivement des logicels libres. En effet, ces derniers permettent de nous assurer que les programmes que nous vous proposons ne collectent pas vos données. Nous nous engageons donc à n'utiliser exclusivement que des logiciels libres.

## Où sont stockées mes données ?

Les serveurs de Mélisse sont hébergés dans le data-center associatif du FAI Aquilenet à Bordeaux https://www.aquilenet.fr, qui nous permettent d'utiliser leur réseau. C'est sur ces disques durs que sont hébergées vos données !

Vos données restent donc intégralement dans l'infrastructure de Mélisse et nous nous engageons nous-mêmes à ne pas les consulter.

## Quelle sécurité mettez-vous en place sur votre infrastructure ?

Nous mettons en place des parefeus et des analyseurs de log (fail2ban) afin de rendre la vie difficile aux intrusions.
Nous avons également des services de sauvegardes journalières pour éviter au maximum la perte de données.
Nous nous assurons également que les communications entre vos terminaux et nos serveurs soient intégralement chiffrées.

## Quel système d'exploitation fait fonctionner vos services ?

Les serveurs de notre infrastructure tournent sous la distribution GNU/Linux NixOS. Certains de nos services tournent dans des conteneurs docker.

---
title: L'association
---

## Mélisse

Mélisse est une association loi 1901 en cours de création. Ses missions principales s'appuient sur:

* La promotion des logiciels libres
* L'hébergement de services web exclusivement basés sur des logiciels libres
* Leur accessibilité au plus grand nombre

Par ces actions, nous souhaitons démocratiser l'utilisation des logiciels libres dans la vie quotidienne des individus et des collectivités, afin de rendre les gens totalement maîtres de leurs données et de leur outil informatique dans un souci éthique. En ce sens, nous nous inscrivons totalement dans la démarche du collectif [CHATONS](https://chatons.org) initié par [Framasoft](https://framasoft.org), auquel nous souhaitons postuler.

<!-- ![CHATONS](../images/logo_chatons_v2.png) -->

### Nous faisons de notre mieux

Mélisse est pour l'instant composé de deux bénévoles qui conçoivent, construisent et supervisent toute l'infrastructure physique et logicielle. Nous faisons de notre mieux pour vous garantir un service optimal. Cependant, utiliser nos services c'est accepter qu'il peut y avoir parfois quelques défaillances, surtout au début. C'est pourquoi il est important de rappeler que nous ne vendons pas un produit, mais que nous vous proposons d'avancer ensemble dans la construction d'une alternative aux GAFAM. En effet, les retours que vous avez à nous faire sur l'utilisation de nos services, bons ou mauvais, contribuent énormément à la fiabilité de la structure sur le long terme.

C'est également ça le logiciel libre : rompre avec l'idée que nos outils doivent faire exactement ce que nous exigeons, pour la remplacer par l'idée que nous fabriquons nous-mêmes nos outils. Et pas besoin de savoir programmer pour participer!

## Aquilenet

[Aquilenet](https://aquilenet.fr) est un fournisseur d'accès Internet basé sur Bordeaux de qui Mélisse est très proche. L'infrastructure de Mélisse est d'ailleurs physiquement hébergée dans le local d'Aquilenet.

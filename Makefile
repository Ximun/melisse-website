HOST=coruscant.melisse.org
DEPLOY_PATH=/srv/www/melisse.org/

deploy:
	/bin/rsync -av public/ ${HOST}:${DEPLOY_PATH} --rsync-path="sudo rsync"
